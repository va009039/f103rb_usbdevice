[L152RE_USBDevice](https://bitbucket.org/va009039/l152re_usbdevice)にマージされました。

# F103RB_USBDevice #
NUCLEO-F103RBのためのUSBデバイススタック(mbed USBDevice)です。

HAL_PCDドライバで実装しています。

### 制限事項 ###
* アイソクロナス転送の最大パケットサイズは192バイトです。
* NUCLEO-F103RBのUSBクロックを48MHzに設定をする必要があります。
* ソフトコネクトはしていません。USB_DPにプルアップ抵抗が必要です。

### 追加・変更ファイル ###
* USBHAL_STM32F1.cpp - 追加
* USBHAL.h - 変更
* USBEndpoints.h - 変更
* USBEndpoints_STM32F1.h - 追加