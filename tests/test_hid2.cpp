// test_hid2.cpp 2015/6/19
#ifdef TEST_HID2
#include "mbed.h"
#include "USBHID.h"
#include "STM32_USB48MHz.h"
 
//This report will contain data to be sent
HID_REPORT send_report;
HID_REPORT recv_report;
 
DigitalOut led1(LED1);
Serial pc(USBTX,USBRX);

int main(void) {
    STM32_HSI_USB48MHz();

    USBHID hid(8, 8);

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);

    send_report.length = 8;
 
    Timer t;
    t.reset();
    t.start();
    while (1) {
        if (t.read_ms() > 1500) {
            t.reset();
            //Fill the report
            for (int i = 0; i < send_report.length; i++) {
                send_report.data[i] = rand() & 0xff;
            }
            //Send the report
            hid.send(&send_report);
        }
        //try to read a msg
        if(hid.readNB(&recv_report)) {
            pc.printf("%d ms recv: ", t.read_ms());
            for(int i = 0; i < recv_report.length; i++) {
                pc.printf("%d ", recv_report.data[i]);
            }
            pc.printf("\n");
            led1 = !led1;
        }
    }
}
#endif // TEST_HID2


