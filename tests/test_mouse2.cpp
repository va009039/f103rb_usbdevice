// test_mouse2.cpp 2015/6/19
#ifdef TEST_MOUSE2

#include "mbed.h"
#include "USBMouse.h"
#include "STM32_USB48MHz.h"

int main() {
    STM32_HSI_USB48MHz();

    USBMouse mouse;

    int16_t x = 0;
    int16_t y = 0;
    int32_t radius = 10;
    int32_t angle = 0;

    while (1) {
        x = cos((double)angle*3.14/180.0)*radius;
        y = sin((double)angle*3.14/180.0)*radius;
        
        mouse.move(x, y);
        angle += 3;
        wait_ms(5);
    }
}

#endif

